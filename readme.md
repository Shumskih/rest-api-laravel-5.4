# Rest API Laravel Backend Project

This is a Rest API Backend Project built with Laravel. Folder 'itemapi' contains the project itself and folder 'itemmanager' contains simple frontend part which communicates with the project.

##### Used backend technologies:
 
- Laravel
- JSON

##### Used frontend technologies:
  
- Bootstrap CSS Framework 
- JQuery